from hdfs.ext.kerberos import KerberosClient

client = KerberosClient('http://hdfs-nn-1.au.adaltas.cloud:50070')
    
client.makedirs('/education/cs_2022_spring_1/s.fadel-cs/Fil_Rouge_Project/PDF_Files')

client.delete('/user/s.fadel-cs/fil_rouge/PDF_Files', recursive=True, skip_trash=False)

client.upload('/user/s.fadel-cs/fil_rouge/PDF_Files', 'PDF/', n_threads=0, temp_dir=None, chunk_size=65536, progress=None, cleanup=True)

for d in client.list('/user/s.fadel-cs/fil_rouge/PDF_Files'):
    print(d)

# Fil Rouge Project

Projects are installed on virtual machine ubuntu (Version 20.04)

First move the zip file to your machine and unzip

```bash
unzip fil_rouge_pdfinfo_extraction-master.zip
```

## Jupyter-lab installation

First we need to install jupyter notebook on ubuntu as **Root user**

```bash
sudo apt-get update
sudo apt install python3-pip
pip3 install jupyter
```

Then we need to install some dependencies for the pdftotext

```bash
sudo apt install build-essential libpoppler-cpp-dev pkg-config python3-dev
```



## Docker installation

Install docker

```bash
sudo apt install docker.io
```

Then run the following to create the docker container

```bash
sudo docker build -t projetfilerouge .
sudo docker run -d -p 8000:5000 projetfilerouge
```



## Run Notebook Code

Connect to Jupyter Lab and go to the directory where you unziped the folder

Open the pdfextractor.ipynb and run the code

Go to owl/ directory and download the onto.owl file and open it in Protege

## Big Data

Create the file /etc/krb5.conf :

```bash
[libdefaults]
 default_ccache_name = FILE:/tmp/krb5cc_%{uid}
 default_realm = AU.ADALTAS.CLOUD
 dns_lookup_realm = false
 dns_lookup_kdc = true
 rdns = false
 ticket_lifetime = 24h
 forwardable = true
 udp_preference_limit = 0

[realms]
 AU.ADALTAS.CLOUD = {
  kdc = ipa1.au.adaltas.cloud:88
  master_kdc = ipa1.au.adaltas.cloud:88
  admin_server = ipa1.au.adaltas.cloud:749
  default_domain = au.adaltas.cloud
 }

[domain_realm]
 .au.adaltas.cloud = AU.ADALTAS.CLOUD
 au.adaltas.cloud = AU.ADALTAS.CLOUD
 ipa1.au.adaltas.cloud = AU.ADALTAS.CLOUD
```

Please install the following first on your local machine

```bash
sudo apt-get install gcc python-dev libkrb5-dev
sudo apt install krb5-user
```

do the following to take a ticket:

```bash
kinit $USER
```

After unzipping the folder, go to Big Data folder

To move the PDF/ file to HDFS plz go to Big_Data/oozie_wf/scripts/hdfs_pdf.py and install the requirements then run the python code

```bash
cd Big_Data/oozie_wf/scripts/
pip3 install -r requirements.txt
python3 hdfs_pdf.py
```

For the second question (Hive) please connect to zeppelin. you already have access to my folder "sandy_fadel". You can view the create table query as well as the dashboards.

For the oozie Part:

Copy the file oozie_wf to the hadoop cluster

then run the following to execute the job

```bash
kinit s.fadel-cs
hdfs dfs -put -f oozie_wf/ "/user/$USER"
oozie job -run -config oozie_wf/job.properties -oozie http://oozie-1.au.adaltas.cloud:11000/oozie
```

you will have a job ID then execute the command to see the status of the job

```bash
oozie job -info 0002230-210916172301405-oozie-oozi-W -oozie http://oozie-1.au.adaltas.cloud:11000/oozie
```


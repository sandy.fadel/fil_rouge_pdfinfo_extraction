from Fil_Rouge_PdfInfo_Extraction import app
from flask import request,jsonify
import spacy
import json

@app.route("/namedentities", methods = ['POST'])
def extract_ref() :
    text = request.json['text']
    spacy_nlp = spacy.load('en_core_web_sm')
    doc = spacy_nlp(text.strip())
    named_entities = []

    for i in doc.ents:
        entry = str(i.lemma_).lower()
        text = text.replace(str(i).lower(), "")
        if i.label_ in ["ART", "EVE", "NAT", "PERSON"]:
            named_entities.append(entry.title().replace(" ", "_").replace("\n","_"))
    return jsonify(named_entities)
